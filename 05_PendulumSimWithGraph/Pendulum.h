#pragma once

class Pendulum
{

private:
	double _bobMass, _bobWeight, _rodLength, _theta;

public:
	Pendulum(double bobMass, double rodLength, double initialAngle);
	~Pendulum();
	
	void setAngle(double theta);
	void setBobWeight(double contGravity);

	double getAngle(void);
	double getBobWeight(void);
	double getRodLength(void);

};

