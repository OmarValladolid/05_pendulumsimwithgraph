#include "CSVWriter.h"
#include <fstream>

CSVWriter::CSVWriter(string fileName, string delimiter = ",")
{
	this->_fileName = fileName;
	this->_delimiter = delimiter;
	this->_lineCounter = 0;
}

CSVWriter::~CSVWriter() {}

void CSVWriter::AddHeaderColum(string newColumn)
{
	//vector<string> header = { "Frame#", "DeltaTime", "EulerIntValue", "SetPoint", "Error", "Output", "OldAngle", "Angle", "Kp", "Ki", "Kd", "OldTheta", "OldAlpha", "OldBeta", "NewTheta", "NewAlpha", "NewBeta", "OldBicepsForce", "NewBicepsForce", "Time", "BallMass" };
	this->_header.push_back(newColumn);
}

void CSVWriter::WriteHeader()
{
	this->WriteLine(this->_header.begin(), this->_header.end());
}

void CSVWriter::WriteTempLine(void)
{
	this->WriteLine(this->_tmpTextLine.begin(), this->_tmpTextLine.end());
}

template<typename T>
void CSVWriter::WriteLine(T first, T last)
{
	fstream file;
	file.open(_fileName, ios::out | (this->_lineCounter ? ios::app : ios::trunc));

	if (file)
	{
		for (; first != last;)
		{
			file << *first;
			if (++first != last)
				file << _delimiter;
		}

		file << "\n";
		this->_lineCounter++;

		file.close();
	}
}

void CSVWriter::ClearText()
{
	this->_tmpTextLine.clear();
}

void CSVWriter::AddText(string text)
{
	this->_tmpTextLine.push_back(text);
}

vector<string> CSVWriter::GetText(void)
{
	return this->_tmpTextLine;
}