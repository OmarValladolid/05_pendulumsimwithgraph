#pragma once

struct Point
{
	float x;
	float y;
	float z;
};

struct Color
{
	float r;
	float g;
	float b;
};