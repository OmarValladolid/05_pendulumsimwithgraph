#pragma once

#include "Shader.h"

const unsigned int NumberOfVerticesBob = 45;
const unsigned int NumberOfTriangleIndicesBob = 39; // This is Number of Vertices -6

class Bob
{
private:
	float _radius;
	float _vertices[NumberOfVerticesBob];
	float _colors[NumberOfVerticesBob];
	unsigned int _triangles[NumberOfTriangleIndicesBob];
	unsigned int _vbo[2], _vao, _ebo;

	void setVertices(void);

public:
	Bob(float radius);
	~Bob();
	void buildGeometry(Shader* shader);
	void render(void);
};