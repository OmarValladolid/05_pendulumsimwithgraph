#pragma once

#include "Shader.h"

// 4 vertices, x,y,z per vertex = 12 elements in the array
// adding color per vertex, r,g,b per vertex, will be 24
const unsigned int NumberOfVertices = 24;
const unsigned int NumberOfTriangleIndices = 6;

class Rod
{
private:
	float _vertices[NumberOfVertices];
	unsigned int _triangles[NumberOfTriangleIndices];
	unsigned int _vbo, _vao, _ebo;

	void setVertices(void);

public:
	Rod();
	~Rod();
	void buildGeometry(Shader* shader);
	void render(void);
};

