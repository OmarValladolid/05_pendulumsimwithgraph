// Glad and GLFW Libraries
#include <glad/glad.h>
#include <GLFW/glfw3.h>

// GLM Libraries
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

// Own Libraries
#include "Scalars.h"
#include "Structs.h"
#include "Shader.h"
#include "Rod.h"
#include "Bob.h"
#include "LineSeries.h"
#include "Pendulum.h"
#include "CSVWriter.h"
#include "camera.h"


// C++ Libraries
#include <iostream>
#include <thread>
#include <vector>
#include <mutex>


using namespace std;
// Screen Settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;
bool stop = false;

struct PhysicValues
{
    double time;
    double theta;	// in rad
    double angularAcc;	// in 
    double angularVel;	// in 
} physicsValues;


static std::mutex  mtx;
std::mutex mtxEuler;
std::mutex mtxWriter;
std::mutex mtxSim;
std::mutex mtxPlot;

// Time variables
double currentTime;
double previousTime;
double displayDeltaTime;
double displayTime;
double initialTime;
double timeAccumulated;
double plotTime;

// Simulation variables
double angularAcc, newAngularVel;
double physicsTime;
unsigned int physicsTick;
double physicsDeltaTime;
double calculationDeltaTime;

// Vectors
// Simulation data
vector<PhysicValues> simulationData;
vector<Point> angularAccData;
vector<Point> angularVelData;
vector<Point> thetaData;
// Color data
vector<Color> angularAccColor;
vector<Color> angularVelColor;
vector<Color> thetaColor;


// Objects
Shader * rodShader, * bobShader, * accShader, * velShader, * thetaShader;
Rod * rod;
Bob * bob;
LineSeries * angularAccLS;
LineSeries * angularVelLS;
LineSeries * thetaLS;
Pendulum * pendulum;
CSVWriter* simulationFile;
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));

// Functions 
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow* window);

void initGLFW(void);
bool initWindow(GLFWwindow* window);
bool initGLAD(void);
void init(void);
void display(GLFWwindow* window);
void plotSeries(Shader* shader, LineSeries* lineSerie);

// Simulation funtions
void timeSimulation(void);
void runSimulation(void);
double improvedEulerIntegration(double dt, double currentTheta);

// File writer functions
void setSimulationFileHeader(void);
void writeSimulationFileHeader(void);
Point createPoint(double x, double y, double z);
Color createColor(float r, float g, float b);

int main()
{
    initGLFW();

    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Pendulum Simulation", NULL, NULL);

    if (!initWindow(window))
        return -1;

    if (!initGLAD())
        return -1;

    // Objects initialization
    init();

    rod->buildGeometry(rodShader);
    bob->buildGeometry(bobShader);
    angularAccLS->buildGeometry(accShader);


    //glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
    glm::mat4 projection = glm::ortho(-1.0f, 1.0f, 0.0f, 2.0f, 0.0f, 10.0f);
    glm::mat4 view = camera.GetViewMatrix(); // position, target, up vector

    rodShader->use();
    rodShader->setMat4("projection", projection);
    rodShader->setMat4("view", view);
    
    bobShader->use();
    bobShader->setMat4("projection", projection);
    bobShader->setMat4("view", view);

    // Simulation decoupled from the display
    thread simulation(runSimulation);
    
    // We wait some time to accumulate simulation data
    this_thread::sleep_for(chrono::milliseconds(2000));

    // render loop
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        // input
        // -----
        processInput(window);

        // Time simulation
        timeSimulation();
        
        // render
        // ------
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glViewport(0, (SCR_HEIGHT / 2), SCR_WIDTH, SCR_HEIGHT);
        //glClearColor(0.65f, 0.77f, 0.85f, 1.0f);
        glClearColor(0.55f, 0.67f, 0.75f, 1.0f);
        display(window);

        //glEnable(GL_SCISSOR_TEST);
        glViewport(0, 0, SCR_WIDTH, (SCR_HEIGHT / 2));
        //glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glLineWidth(2.0);
        angularAccLS->updatePoints(angularAccData, angularAccColor, accShader);
        plotSeries(accShader, angularAccLS);

        angularVelLS->updatePoints(angularVelData, angularVelColor, velShader);
        plotSeries(velShader, angularVelLS);

        thetaLS->updatePoints(thetaData, thetaColor, thetaShader);
        plotSeries(thetaShader, thetaLS);

        glLineWidth(1.0);

        glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
        glfwSwapBuffers(window);
        glfwPollEvents();

    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    rod->~Rod();
    bob->~Bob();
    angularAccLS->~LineSeries();
    angularVelLS->~LineSeries();
    thetaLS->~LineSeries();

    simulationData.clear();
    angularAccData.clear();
    angularVelData.clear();
    thetaData.clear();

    angularAccColor.clear();
    angularVelColor.clear();
    thetaColor.clear();

    if (simulation.joinable()) simulation.join();

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}

/// <summary>
/// Initialize and configure GLFW
/// </summary>
void initGLFW(void)
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
}

/// <summary>
/// GLFW window creation
/// </summary>
/// <returns>Window creation result</returns>
bool initWindow(GLFWwindow* window)
{
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return false;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    return true;
}

/// <summary>
/// Load all OpenGL function pointers
/// </summary>
/// <returns>GLAD initialization result</returns>
bool initGLAD(void)
{
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return false;
    }

    return true;
}

/// <summary>
/// Process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
/// </summary>
/// <param name="window">GLFW Window object</param>
void processInput(GLFWwindow* window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        stop = true;
        glfwSetWindowShouldClose(window, true);
    }
}

/// <summary>
/// Whenever the window size changed (by OS or user resize) this callback function executes
/// </summary>
/// <param name="window">GLFW Window object</param>
/// <param name="width">Window width</param>
/// <param name="height">Window height</param>
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
    
}

void init(void)
{
    // CSV Writer object
    simulationFile = new CSVWriter("PendulumSimulation.csv", ",");
    setSimulationFileHeader();

    // Simulation variables
    currentTime = 0;
    previousTime = 0;
    displayTime = -1;
    displayDeltaTime = 0;
    initialTime = 0;
    timeAccumulated = 0;
    angularAcc = 0;
    newAngularVel = 0;
    physicsTime = 0;
    physicsTick = 1000;
    physicsDeltaTime = 0.01;
    calculationDeltaTime = physicsDeltaTime / (double)physicsTick;
    plotTime = 0;

    // Pendulum object
    pendulum = new Pendulum(1.5, 0.26, glm::radians(45.0f));
    pendulum->setBobWeight(g);

    physicsValues.time = physicsTime;
    physicsValues.theta = pendulum->getAngle();
    physicsValues.angularAcc = angularAcc;
    physicsValues.angularVel = newAngularVel;

    // Simulation Data
    simulationData.push_back(physicsValues);

    // Initial values for line series
    angularAccData.push_back(createPoint(physicsTime, angularAcc, 0));
    angularVelData.push_back(createPoint(physicsTime, newAngularVel, 0));
    thetaData.push_back(createPoint(physicsTime, pendulum->getAngle(), 0));

    angularAccColor.push_back(createColor(1.0f, 0.0f, 0.0f));
    angularVelColor.push_back(createColor(0.0f, 1.0f, 0.0f));
    thetaColor.push_back(createColor(0.0f, 0.0f, 1.0f));

    // build and compile our shader program
    // ------------------------------------
    rodShader = new Shader("glslFiles/shader.vert", "glslFiles/shader.frag"); // you can name your shader files however you like
    bobShader = new Shader("glslFiles/shader.vert", "glslFiles/shader.frag");
    accShader = new Shader("glslFiles/shader.vert", "glslFiles/shader.frag");
    velShader = new Shader("glslFiles/shader.vert", "glslFiles/shader.frag");
    thetaShader = new Shader("glslFiles/shader.vert", "glslFiles/shader.frag");

    // Objects to draw
    rod = new Rod();
    bob = new Bob(0.05f);
    angularAccLS = new LineSeries(angularAccData, angularAccColor);
    angularVelLS = new LineSeries(angularVelData, angularVelColor);
    thetaLS = new LineSeries(thetaData, thetaColor);

}

void display(GLFWwindow * window)
{
    // Clear color
    /*glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);*/

    // Rod shader and object render
    rodShader->use();
    glm::mat4 model = glm::mat4(1.0f);
    // Rod rotation considering the upper end as its origin.
    model = glm::translate(model, glm::vec3(0.0f, 0.9f, 0.0f));
    model = glm::rotate(model, (float)pendulum->getAngle(), glm::vec3(0.0, 0.0, 1.0));
    model = glm::translate(model, glm::vec3(0.0f, -0.9f, 0.0f));
    unsigned int modelLoc = rodShader->getUniformLocation("model");
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

    rod->render();

    
    // Bod shader and object render
    bobShader->use();
    glm::mat4 bobModel = glm::mat4(1.0f);
    // Transformations to rotate Bob considering the same origin as the Rod
    bobModel = glm::translate(bobModel, glm::vec3(0.0f, 0.9f, 0.0f));
    bobModel = glm::rotate(bobModel, (float)pendulum->getAngle(), glm::vec3(0.0, 0.0, 1.0));
    bobModel = glm::translate(bobModel, glm::vec3(0.0f, -0.9f, 0.0f));
    // Tranlate at the end of the Rod
    bobModel = glm::translate(bobModel, glm::vec3(0.0f, 0.5f, 0.0f));
    unsigned int bobModelLoc = bobShader->getUniformLocation("model");
    glUniformMatrix4fv(bobModelLoc, 1, GL_FALSE, glm::value_ptr(bobModel));
    bob->render();
    
    // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
    /*glfwSwapBuffers(window);
    glfwPollEvents();*/
}

void plotSeries(Shader * shader, LineSeries * lineSerie)
{
    std::lock_guard<mutex> lock(mtxPlot);

    shader->use();
    // Set the orthographic projection
    glm::mat4 projection = glm::ortho(-1.0f + (float)plotTime, 30.0f + (float)plotTime, -50.0f, 50.0f, 0.0f, 10.0f);
    glm::mat4 view = camera.GetViewMatrix(); // position, target, up vector

    shader->setMat4("projection", projection);
    shader->setMat4("view", view);

    glm::mat4 lineModel = glm::mat4(1.0f);

    shader->setMat4("model", lineModel);
    lineSerie->render();
}

void timeSimulation(void)
{
    std::lock_guard<mutex> lock(mtxSim);

    currentTime = glfwGetTime();
    displayDeltaTime = currentTime - previousTime;
    //timeAccumulated += displayDeltaTime;

    if (displayTime == -1)
    {
        initialTime = currentTime;
        displayTime = 0;
    }
    else
    {
        //displayTime = currentTime - initialTime;
        displayTime += 0.01;
        timeAccumulated += 0.01;

        if (timeAccumulated >= 0.03)
            timeAccumulated = 0;
        else
            return;

        if (displayTime >= 25)
            plotTime = displayTime - 25;

        double index = 0;
        double fraction = modf(displayTime / physicsDeltaTime, &index);

        PhysicValues current, previous;
        
        if(index == 0)
            previous = simulationData.at(0);
        else
            if (index > simulationData.size())
            {
                previous = simulationData.at(simulationData.size() - 1);
                cout << "Display, No data. Id: " << simulationData.size() - 1 << endl;
            }
            else
                previous = simulationData.at((int)index - 1);

        if (fraction > 0 && index < simulationData.size())
        {
            current = simulationData.at((int)index);

            double tempAngularAcc = 0;
            double tempAngularVel = 0;
            double tempTheta = 0;

            tempAngularAcc = ((current.time * previous.angularAcc) + (previous.time * current.angularAcc)) / (previous.time + current.time);
            tempAngularVel = ((current.time * previous.angularVel) + (previous.time * current.angularVel)) / (previous.time + current.time);
            tempTheta = ((current.time * previous.theta) + (previous.time * current.theta)) / (previous.time + current.time);

            angularAccData.push_back(createPoint(displayTime, tempAngularAcc, 0));
            angularVelData.push_back(createPoint(displayTime, tempAngularVel, 0));
            thetaData.push_back(createPoint(displayTime, tempTheta, 0));

            angularAccColor.push_back(createColor(1.0f, 0.0f, 0.0f));
            angularVelColor.push_back(createColor(0.65f, 0.93f, 0.54f));
            thetaColor.push_back(createColor(0.0f, 0.0f, 1.0f));

            pendulum->setAngle(tempTheta);
        }
        else
        {
            angularAccData.push_back(createPoint(displayTime, previous.angularAcc, 0));
            angularVelData.push_back(createPoint(displayTime, previous.angularVel, 0));
            thetaData.push_back(createPoint(displayTime, previous.theta, 0));

            angularAccColor.push_back(createColor(1.0f, 0.0f, 0.0f));
            angularVelColor.push_back(createColor(0.0f, 1.0f, 0.0f));
            thetaColor.push_back(createColor(0.0f, 0.0f, 1.0f));

            pendulum->setAngle(previous.theta);
        }
    }
    
    cout << "Current Time: " << currentTime << endl;
    cout << "Display time: " << displayTime << endl;
    cout << "Display DT: " << displayDeltaTime << endl;
    cout << "Plot time: " << plotTime << endl;

    previousTime = currentTime;
}

void runSimulation(void)
{
    // Simulation calculations
    double tmpTheta = pendulum->getAngle(); // Starts with the initial theta's value

    do
    {
        //std::lock_guard<mutex> lock(mtx);
        mtx.lock();
        
        physicsTime += physicsDeltaTime;

        for (unsigned int i = 0; i < physicsTick; i++)
        {
            tmpTheta = improvedEulerIntegration(calculationDeltaTime, tmpTheta);
        }

        physicsValues.time = physicsTime;
        physicsValues.angularAcc = angularAcc;
        physicsValues.angularVel = newAngularVel;
        physicsValues.theta = tmpTheta;

        cout << "Simulation, Physics Time: " << physicsTime << endl;

        // Add data to vector
        simulationData.push_back(physicsValues);

        // Write data to CSV File
        writeSimulationFileHeader();
        
        mtx.unlock();

    } while (!stop);
    cout << "Finish simulation" << endl;
}

double improvedEulerIntegration(double dt, double currentTheta)
{
    std::lock_guard<mutex> lock(mtxEuler);
    double angularVelocity = 0.0f, oldTheta = 0.0f, newTheta = 0.0f;

    angularAcc = -(g / pendulum->getRodLength()) * sin(currentTheta);

    angularVelocity = newAngularVel;
    oldTheta = currentTheta;

    // New angular velocity
    newAngularVel = angularVelocity + (angularAcc * dt);
    // New angle
    newTheta = oldTheta + (((angularVelocity + newAngularVel) / 2.0) * dt);

    return newTheta; // value in rad
}

void setSimulationFileHeader(void)
{
    simulationFile->AddHeaderColum("Time");
    simulationFile->AddHeaderColum("DisplayTime");
    simulationFile->AddHeaderColum("DisplayTimeStep");
    simulationFile->AddHeaderColum("SimulationTime");
    simulationFile->AddHeaderColum("SimulationTimeStep");
    simulationFile->AddHeaderColum("CalculationTimeStep");
    simulationFile->AddHeaderColum("BobMass");
    simulationFile->AddHeaderColum("RodLength");
    simulationFile->AddHeaderColum("Theta");
    simulationFile->AddHeaderColum("AngularAcc");
    simulationFile->AddHeaderColum("AngularVel");

    simulationFile->WriteHeader();
}

void writeSimulationFileHeader(void) 
{
    std::lock_guard<mutex> lock(mtxWriter);
    // Add data to file
    simulationFile->ClearText();
    simulationFile->AddText(to_string(physicsTime));
    simulationFile->AddText(to_string(displayTime));
    simulationFile->AddText(to_string(displayDeltaTime));
    simulationFile->AddText(to_string(physicsTime));
    simulationFile->AddText(to_string(physicsDeltaTime));
    simulationFile->AddText(to_string(calculationDeltaTime));
    simulationFile->AddText(to_string(pendulum->getBobWeight()));
    simulationFile->AddText(to_string(pendulum->getRodLength()));
    simulationFile->AddText(to_string(physicsValues.theta));
    simulationFile->AddText(to_string(physicsValues.angularAcc));
    simulationFile->AddText(to_string(physicsValues.angularVel));

    simulationFile->WriteTempLine();
}

Point createPoint(double x, double y, double z)
{
    Point pointData;
    pointData.x = (float)x;
    pointData.y = (float)y;
    pointData.z = (float)z;

    return pointData;
}

Color createColor(float r, float g, float b)
{
    Color colorData;
    colorData.r = r;
    colorData.g = g;
    colorData.b = b;

    return colorData;
}