#include "Bob.h"
#include "Scalars.h"

Bob::Bob(float radius)
{
    this->_radius = radius;
    this->_vao = 0;
    this->_vbo[0] = 0;
    this->_vbo[1] = 0;
    this->_ebo = 0;
    setVertices();
}

Bob::~Bob()
{
    glDeleteVertexArrays(1, &this->_vao);
    glDeleteBuffers(1, &this->_vbo[0]);
    glDeleteBuffers(1, &this->_vbo[1]);
    glDeleteBuffers(1, &this->_ebo);
}

void Bob::buildGeometry(Shader* shader)
{
    glGenVertexArrays(1, &this->_vao);
    glGenBuffers(2, this->_vbo);
    glGenBuffers(1, &this->_ebo);
    // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(this->_vao);

    // Position attribute
    glBindBuffer(GL_ARRAY_BUFFER, this->_vbo[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(this->_vertices), this->_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(shader->getAttributeLocation("inPos"), 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    // Color attribute
    glBindBuffer(GL_ARRAY_BUFFER, this->_vbo[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(this->_colors), this->_colors, GL_STATIC_DRAW);
    glVertexAttribPointer(shader->getAttributeLocation("inColor"), 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(1);

    // Element buffer object
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->_ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(this->_triangles), this->_triangles, GL_STATIC_DRAW);

    glBindVertexArray(0);
}

void Bob::setVertices(void)
{
    for (unsigned int i = 0, counter = 0; i < NumberOfVerticesBob; i+=3, counter++)
    {
        double radValue = (double)counter * 2 * PI / ((double)NumberOfVerticesBob/3);
        this->_vertices[i] = (float)cos(radValue) * this->_radius; // X coordinate
        this->_vertices[i + 1] = (float)sin(radValue) * this->_radius; // Y coordinate
        this->_vertices[i + 2] = 0; // Z coordinate

        // Set color per vertex
        this->_colors[i] = 0.63f; // Red level
        this->_colors[i + 1] = 0.52f; // Green level
        this->_colors[i + 2] = 0.64f; // Blue level
    }

    for (unsigned int i = 0, counter = 0; i < NumberOfTriangleIndicesBob; i+=3, counter++)
    {
        if (i == 0)
        {
            this->_triangles[i] = counter;
            this->_triangles[i + 1] = counter + 1;
            this->_triangles[i + 2] = counter + 2;
            counter += 2;
        }
        else
        {
            this->_triangles[i] = counter - counter;
            this->_triangles[i + 1] = counter - 1;
            this->_triangles[i + 2] = counter;
        }
    }
}

void Bob::render(void)
{
    glBindVertexArray(this->_vao);
    glDrawElements(GL_TRIANGLES, NumberOfTriangleIndicesBob, GL_UNSIGNED_INT, 0);
}