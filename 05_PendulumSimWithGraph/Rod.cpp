#include "Rod.h"

Rod::Rod()
{
    this->_vao = 0;
    this->_vbo = 0;
    this->_ebo = 0;
    setVertices();
}

Rod::~Rod()
{
    glDeleteVertexArrays(1, &this->_vao);
    glDeleteBuffers(1, &this->_vbo);
    glDeleteBuffers(1, &this->_ebo);
}

void Rod::buildGeometry(Shader* shader)
{
    glGenVertexArrays(1, &this->_vao);
    glGenBuffers(1, &this->_vbo);
    glGenBuffers(1, &this->_ebo);
    
    // Bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(this->_vao);

    glBindBuffer(GL_ARRAY_BUFFER, this->_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(this->_vertices), this->_vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->_ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(this->_triangles), this->_triangles, GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(shader->getAttributeLocation("inPos"), 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    // Color attribuute
    glVertexAttribPointer(shader->getAttributeLocation("inColor"), 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    
    glBindVertexArray(0);
}

void Rod::setVertices(void)
{
    // Vertices of the rod
    // Top left
    this->_vertices[0] = -0.01f; this->_vertices[1] = 0.9f; this->_vertices[2] = 0.0f; // Vertex 1
    this->_vertices[3] = 0.38f; this->_vertices[4] = 0.35f; this->_vertices[5] = 0.43f; // Color vertex 1

    // Top right
    this->_vertices[6] = 0.01f; this->_vertices[7] = 0.9f; this->_vertices[8] = 0.0f; // Vertex 2
    this->_vertices[9] = 0.38f; this->_vertices[10] = 0.35f; this->_vertices[11] = 0.43f; // Color vertex 2

    // Bottom right
    this->_vertices[12] = 0.01f; this->_vertices[13] = 0.5f; this->_vertices[14] = 0.0f; // Vertex 3
    this->_vertices[15] = 0.38f; this->_vertices[16] = 0.35f; this->_vertices[17] = 0.43f; // Color vertex 3

    // Bottom left
    this->_vertices[18] = -0.01f; this->_vertices[19] = 0.5f; this->_vertices[20] = 0.0f; // Vertex 4
    this->_vertices[21] = 0.38f; this->_vertices[22] = 0.35f; this->_vertices[23] = 0.43f; // Color vertex 4

    // Vertices of the triangles
    this->_triangles[0] = 0; this->_triangles[1] = 1; this->_triangles[2] = 2; // First triangle
    this->_triangles[3] = 2; this->_triangles[4] = 3; this->_triangles[5] = 0; // Second triangle
}

void Rod::render(void)
{
    glBindVertexArray(this->_vao);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}