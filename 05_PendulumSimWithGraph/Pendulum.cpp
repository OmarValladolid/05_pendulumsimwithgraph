#include "Pendulum.h"


Pendulum::Pendulum(double bobMass, double rodLength, double initialAngle)
{
	this->_bobMass = bobMass;
	this->_rodLength = rodLength;
	this->_theta = initialAngle;
	this->_bobWeight = 0;
}

Pendulum::~Pendulum()
{
}

void Pendulum::setAngle(double theta)
{
	this->_theta = theta;
}

void Pendulum::setBobWeight(double g)
{
	this->_bobWeight = this->_bobMass * g;
}

double Pendulum::getAngle(void)
{
	return this->_theta;
}

double Pendulum::getBobWeight(void)
{
	return this->_bobWeight;
}

double Pendulum::getRodLength(void)
{
	return this->_rodLength;
}