#pragma once

#include <vector>
#include "Structs.h"
#include "Shader.h"

using namespace std;

class LineSeries
{
private:
	unsigned int _vao;
	unsigned int _vbo[2];
	vector<Point> _points;
	vector<Color> _colors;
	double _xMax, _xMin;
	double _yMax, _yMin;

public:
	LineSeries(vector<Point> pointsVector, vector<Color> colorsVector);
	~LineSeries();
	void buildGeometry(Shader * shader);
	void updatePoints(vector<Point> pointsVector, vector<Color> colorsVector, Shader* shader);
	void render(void);
	void setYMax(double value);
	void setYMin(double value);
	double getYMax(void);
	double getYMin(void);
};

