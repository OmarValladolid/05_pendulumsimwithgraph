#include "LineSeries.h"

LineSeries::LineSeries(vector<Point> pointsVector, vector<Color> colorsVector)
{
	this->_vao = 0;
	this->_vbo[0] = 0;
    this->_vbo[1] = 0;
    this->_points = pointsVector;
    this->_colors = colorsVector;
}

LineSeries::~LineSeries()
{
	glDeleteVertexArrays(1, &this->_vao);
	glDeleteBuffers(1, &this->_vbo[0]);
    glDeleteBuffers(1, &this->_vbo[1]);
}

void LineSeries::buildGeometry(Shader* shader)
{
    // Create Vertex Array and Buffers
    glGenVertexArrays(1, &this->_vao);
    glGenBuffers(2, this->_vbo);
    
    // Bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(this->_vao);


    // Position attribute
    glBindBuffer(GL_ARRAY_BUFFER, this->_vbo[0]);
    glBufferData(GL_ARRAY_BUFFER, this->_points.size() * sizeof(Point), &this->_points[0], GL_STATIC_DRAW);
    glVertexAttribPointer(shader->getAttributeLocation("inPos"), 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    // Color attribute
    glBindBuffer(GL_ARRAY_BUFFER, this->_vbo[1]);
    glBufferData(GL_ARRAY_BUFFER, this->_colors.size() * sizeof(Point), &this->_colors[0], GL_STATIC_DRAW);
    glVertexAttribPointer(shader->getAttributeLocation("inColor"), 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(1);

    //glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
}

void LineSeries::updatePoints(vector<Point> pointsVector, vector<Color> colorsVector, Shader * shader)
{
	this->_points = pointsVector;
    this->_colors = colorsVector;

    this->buildGeometry(shader);
}

void LineSeries::render(void)
{
	glBindVertexArray(this->_vao);
    glDrawArrays(GL_LINE_STRIP, 0, this->_points.size() - 1);
}

void LineSeries::setYMax(double value)
{
    this->_yMax = value;
}

void LineSeries::setYMin(double value)
{
    this->_yMin = value;
}

double LineSeries::getYMax(void)
{
    return this->_yMax;
}

double LineSeries::getYMin(void)
{
    return this->_yMin;
}